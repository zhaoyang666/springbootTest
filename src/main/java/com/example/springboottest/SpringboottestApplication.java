package com.example.springboottest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
//@EnableScheduling	//springboot定时任务
@EnableAsync	//开启异步调用
@EnableCaching	//开启注解
public class SpringboottestApplication {
	//访问路径http://localhost:8011/sbtest/showPerson
	public static void main(String[] args) {
		SpringApplication.run(SpringboottestApplication.class, args);
	}
}
