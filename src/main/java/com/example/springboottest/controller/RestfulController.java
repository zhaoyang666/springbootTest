package com.example.springboottest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestfulController {
    private Logger logger = LoggerFactory.getLogger(RestfulController.class);
    @Autowired
    private RestTemplate restTemplatet;

    @RequestMapping("/rest1")
    public String rest1(){
        String str = restTemplatet.getForObject("http://gturnquist-quoters.cfapps.io/api/random", String.class);
        return "return:"+str;

    }
    @RequestMapping("rest2")
    public String rest2(){
        String url = String.format("https://api.github.com/users/%s", "PivotalSoftware");
        System.out.println(url);
        return url;
    }
}
