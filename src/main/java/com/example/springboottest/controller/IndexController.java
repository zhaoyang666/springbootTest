package com.example.springboottest.controller;

import com.example.springboottest.config.AsyncTask;
import com.example.springboottest.dto.Dog;
import com.example.springboottest.dto.PersonDto;
import com.example.springboottest.service.IndexService;
import com.example.springboottest.service.PersonService;
import com.example.springboottest.util.SpringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

@Controller

public class IndexController {
    private Logger logger = LoggerFactory.getLogger(IndexController.class);
    @Resource
    private IndexService indexService;
    @Resource
    private PersonService personService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("person", new PersonDto(1, "柱子", "卡通！"));
        model.addAttribute("date", new Date());
        System.out.println("进入controller方法。。。");
        return "hello";
    }
//#########################################################################################
    /**
     * 获取配置bean类中（GetBean）配置的bean，通过工具类（SpringUtil）
     */
    @RequestMapping("getbean")
    @ResponseBody
    public String getbean() {
        PersonDto person = (PersonDto) SpringUtil.getBean("person1");
        System.out.println(person);
        return "getbean succ...";
    }

//#########################################################################################
    /**
     * 异步任务调用
     */
    @Autowired
private AsyncTask async;
    @RequestMapping("/async")
    @ResponseBody
    public String async() throws InterruptedException {
        long begintime = System.currentTimeMillis();
        Future<String> future1 = async.task1();
        Future<String> future2 = async.task2();
        Future<String> future3 = async.task3();
        while(true) {
            if(future1.isDone() && future2.isDone() && future3.isDone())
                break;
            Thread.sleep(200);
        }
        long endtime = System.currentTimeMillis();
        System.out.println("task任务耗时："+(endtime-begintime)+"ms");
        return "async succ...";
    }
//#########################################################################################

    @RequestMapping("log")
    @ResponseBody
    public String log() {
        logger.debug("这是debug信息");
        logger.info("这是info信息");
        logger.warn("这是warn信息");
        logger.error("这是error信息");
        return "log";
    }

    //#########################################################################################
    @RequestMapping("err")
    @ResponseBody
    public String err() {
        int i = 1 / 0;
        return "aa";
    }

    //#########################################################################################
    //转发和重定向
    @RequestMapping("redirect")
    public String redicect() {
//      return "redirect:/showPerson";  //重定向到映射
//      return "forward:/index";        //转发
        return "redirect:views/addPage.html";  //重定向到页面需要写static下的页面全路径
    }

    @RequestMapping("forward")
    public ModelAndView forward(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("进入方法222");
        try {
            // request.getRequestDispatcher("index").forward(request,response);//转发
            // response.sendRedirect(request.getContextPath()+"/showPerson");//重定向映射
            response.sendRedirect(request.getContextPath() + "/views/addPage.html");//重定向页面
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("test2");   //重定向页面，不需写views
    }
    //#########################################################################################
    /**
     * 文件上传
     */
    @RequestMapping("/upload")
    @ResponseBody
    public String upload(HttpServletRequest request,HttpServletResponse response,MultipartFile file) throws IOException {
        response.setCharacterEncoding("utf-8");
        if(file.isEmpty()){
            return "file can' be null";
        }
        String contextPath = request.getContextPath();
        System.out.println(contextPath);
        String path="D:\\";
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(path+file.getOriginalFilename())));
        bos.write(file.getBytes());
        bos.flush();
        bos.close();
        return "upload succ...";
    }
    //#########################################################################################

    /**
     * 获取redis数据
     */
    @RequestMapping("/getRedis")
    @ResponseBody
    public String test(){
        //获取string
        String name = (String) redisTemplate.opsForValue().get("name");
        System.out.println(name);
        //获取list
        ListOperations<String, String> vo = redisTemplate.opsForList();
        Long size = vo.size("mylist");
        String str="";
        for(int i=0;i<size;i++){
            String index = vo.index("mylist", i);
            str+=index+("...");
        }
        return str;
    }
    //#########################################################################################
}