package com.example.springboottest.controller;

import com.example.springboottest.dto.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 获取资源文件属性配置
 */
@Controller
@Configuration
@PropertySource(value = "classpath:resource.properties")   //文件路径
public class GetResouceAttrController {
    /**
     * 资源文件属性配置
     */
    @Autowired  //①自动注入读取了配置文件属性的实体类
    private Dog dog;

    @Value("${com.source.word}")   //@Value("${com.source.word}")
    private String word;    //②本类中注解读取属性

    @RequestMapping("getvalue")
    @ResponseBody
    public String dogtest2(){
        System.out.println(dog);    //实体类读取配置文件属性
        System.out.println(word);   //本类中直接读取属性
        return "success 2";
    }
}
