package com.example.springboottest.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.springboottest.config.GetBean;
import com.example.springboottest.dto.PersonDto;
import com.example.springboottest.service.PersonService;
import com.example.springboottest.util.FileUtil;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
//访问路径http://localhost:8011/sbtest/showPerson
public class PersonController {
    @Resource
    private PersonService personService;
    @RequestMapping("/testjson")
    @ResponseBody
    public String testjson( ){
        String json="{\"pid\":1,\"pname\":\"等等\",\"skill\":\"11\"}";
        JSONObject jsonObject = JSONObject.parseObject(json);
        String pid = jsonObject.getString("pid");
        PersonDto personDto = JSON.parseObject(json, PersonDto.class);//json转对象
        System.out.println(pid);
        return json;
    }

    //查询
    @RequestMapping("/showPerson")
    public String showPerson(Model model){
        PageHelper.startPage(1,4);  //分页
        List<PersonDto> personList = personService.showAllPerson();
        model.addAttribute("personList",personList);
        //System.out.println(SpringUtil.getBean("person1"));
        return "personPage";
    }
    //删除
    @GetMapping("/delPerson/{id}:hello")
    public String delPerson(@PathVariable(name="id") int pid){
        System.out.println("param is:"+pid);
        /* personService.delPerson(pid);*/
        return "del success";
    }
    //#########################################################################################
    //修改
    @RequestMapping("/upPerson")
    public String upPerson(){
        personService.upPerson(new PersonDto(4,"哈哈","等等"));
        return "up success";
    }
    //批量修改
    @RequestMapping("/batchUp")
    public String batchUp(){
        Map map=new HashMap();
        List list=new ArrayList();
        list.add(new PersonDto(1,"嘻嘻1","嗯嗯1"));
        list.add(new PersonDto(3,"嘻嘻3","嗯嗯3"));
        list.add(new PersonDto(4,"嘻嘻4","嗯嗯4"));
        list.add(new PersonDto(5,"嘻嘻5","嗯嗯5"));
        map.put("list",list);
        personService.batchUp(map);
        return "batchUp success";
    }
    //#########################################################################################
    //body参数
    @PostMapping("/showBody")
    public String showBody(@RequestBody PersonDto personDto){
        System.out.println(personDto);
        return  "succ";
    }

    //#########################################################################################
    //easypoi导出excel
    @RequestMapping("/export")
    public void export(HttpServletResponse response){
        List<PersonDto> personList = personService.showAllPerson();
        FileUtil.exportExcel(personList,"花名册","草帽一伙",PersonDto.class,"海贼王.xlsx",response);
    }
    //easypoi导入excel
    @RequestMapping("import")
    public void importExcel(){
        String filePath = "D:\\海贼王.xlsx";
        //解析excel，
        List<PersonDto> personList = FileUtil.importExcel(filePath,1,1,PersonDto.class);
        System.out.println("导入数据一共【"+personList.size()+"】行");
    }
    //#########################################################################################
    @RequestMapping("insert")
    @ResponseBody
    public String insert(){
        personService.insert2();
        return "insert succ...";
    }

    //批量插入
    @RequestMapping("/insertlist")
    @ResponseBody
    public String insertlist(){
        Map<String, List> map = new HashMap<>();
        ArrayList<PersonDto> personlist = new ArrayList<>();
        for(int i=100;i<20000;i++){
            PersonDto person = new PersonDto(i, "aa" + i, "kk" + i);
            personlist.add(person);
        }
        map.put("list",personlist);
        personService.insertlist(map);
        return "insertlist succ..";
    }

    //#########################################################################################
}
