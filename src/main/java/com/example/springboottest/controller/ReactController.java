package com.example.springboottest.controller;

import com.alibaba.fastjson.JSON;
import com.example.springboottest.dto.PersonDto;
import com.example.springboottest.service.PersonService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * react页面跳转的controller
 */
@RestController
public class ReactController {
    @Resource
    private PersonService personService;

    /**
     *  id查询
     */
    @GetMapping("/showbyidajax/{id}")
    @ResponseBody
    public String showbyidajax(@PathVariable("id") int id){
        PersonDto personDto = personService.showbyid(id);
        String jsonString = JSON.toJSONString(personDto);
        System.out.println(jsonString);
        return jsonString;
    }

    /**
     * 查询所有
     */
    @GetMapping("/showajax")
    @ResponseBody
    public String showajax(){
        System.out.println("1");
        List<PersonDto> list = personService.showAllPerson();
        String jsonString = JSON.toJSONString(list);
        return jsonString;
    }

    /**
     * 新增
     */
    @PostMapping("/addajax")
    @ResponseBody
    public String addajax(@RequestBody PersonDto personDto){
        personService.insert(personDto);
        return null;
    }
    /**
     *  修改
     */
    @RequestMapping("/updateajax")
    public String updateajax(@RequestBody PersonDto personDto){
        personService.upPerson(personDto);
        return null;
    }

    /**
     * 删除
     */
    @DeleteMapping("/delajax/{id}")
    @ResponseBody
    public String delajax(@PathVariable("id") int id){
        personService.delPerson(id);
        List<PersonDto> list = personService.showAllPerson();
        String jsonString = JSON.toJSONString(list);
        return jsonString;
    }
}
