package com.example.springboottest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Scheduled定时方法
 */
@Component
public class ScheduledService {
    private static final Logger log = LoggerFactory.getLogger(ScheduledService.class);
    //@Scheduled(cron="0 37 21 ? * MON-FRI")     //周一到周五10点20
    // @Scheduled(cron="0/15 * * * * ? ")        //每15秒执行一次
    // @Scheduled(fixedRate = 2000)              //每两秒执行一次
    public void cron() throws Exception {
        System.out.println("方法1：" + new Date());
    }
//    @Scheduled(fixedRate = 5000)
    public void repoetCurrentTime(){
//        System.out.println(".............."+new Date());
        log.info("The time is now {}",new SimpleDateFormat("HH:mm:ss").format(new Date()));
    }
}
