package com.example.springboottest.service;

import com.example.springboottest.dto.PersonDto;
import com.example.springboottest.mapper.PersonMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class PersonService {
    @Resource
    private PersonMapper personMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    private final String  KEY="personList66666";    //redis所有用户列表key
    //#########################################################################################
    //查询
//    @Cacheable(cacheNames = "person",key = "123")
    public List<PersonDto> showAllPerson(){

        Boolean bool = redisTemplate.hasKey(KEY);
        if(bool){
            log.info("从缓存读数据..............");
            List list = redisTemplate.opsForList().range(KEY, 0, -1);
            return list;
        }
        List<PersonDto> personList = personMapper.showAllPerson();
        redisTemplate.opsForList().rightPushAll(KEY,personList);
       /* for (PersonDto  x : personList ) {
            redisTemplate.opsForList().rightPush(key,x);
        }*/
        redisTemplate.expire(KEY,30, TimeUnit.SECONDS);
        log.info("调用数据库......写入redis缓存..............");
        return personList;
    }
    public PersonDto showbyid(int pid){
       return personMapper.showbyid(pid);
    }
    //#########################################################################################
    //新增
    public void insert(PersonDto personDto){
        personMapper.insert(personDto);
        Boolean hasKey = redisTemplate.hasKey(KEY);
        if(hasKey){
            redisTemplate.opsForList().rightPush(KEY,personDto);
            log.info("[KEY存在,插入新值到redis...]");
        }
    }
    //#########################################################################################
    //删除
    public void delPerson(int pid){
        try {
            personMapper.delPerson(pid);
            if(redisTemplate.hasKey(KEY)) {
                redisTemplate.delete(KEY);
            }
        } catch (Exception e) {
            System.out.println("删除失败。。");
            e.printStackTrace();
        }
    }
    //#########################################################################################
    //修改
    public void upPerson(PersonDto person){
        personMapper.upPerson(person);
        if(redisTemplate.hasKey(KEY)){
            redisTemplate.delete(KEY);
        }
    }
    //#########################################################################################
    //批量修改
    public void batchUp(Map map){
        personMapper.batchUp(map);
    }
    //#########################################################################################
    //新增,事物管理
    @Transactional(propagation = Propagation.REQUIRED)  //错误了回滚
    public void insert2(){
        PersonDto person1= new PersonDto(11, "试试", "看看");
        PersonDto person2= new PersonDto(12, "试试2", "看看2");
        personMapper.insert(person1);
        int i=3/0;
        personMapper.insert(person2);
    }
    //#########################################################################################
    //批量导入
    public void insertlist(Map map){
        personMapper.insertlist(map);
    }
}
