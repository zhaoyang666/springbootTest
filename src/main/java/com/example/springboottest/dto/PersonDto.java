package com.example.springboottest.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

public class PersonDto implements Serializable {
    @Excel(name = "id",orderNum = "0")
    private int pid;
    @Excel(name = "姓名",orderNum = "0")
    private String pname;
    @Excel(name = "技能",orderNum = "0")
    private String skill;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "PersonDto{" +
                "pid=" + pid +
                ", pname='" + pname + '\'' +
                ", skill='" + skill + '\'' +
                '}';
    }

    public PersonDto(int pid, String pname, String skill) {
        this.pid = pid;
        this.pname = pname;
        this.skill = skill;
    }

    public PersonDto() {

    }
}
