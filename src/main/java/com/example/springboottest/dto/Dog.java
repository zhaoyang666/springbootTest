package com.example.springboottest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@ConfigurationProperties(prefix = "com.source.dog") //属性前缀
@PropertySource(value = "classpath:resource.properties")   //文件路径
//@Setter //自动生成setter、getter方法
//@Getter @ToString
@Data   //自动生成set/get方法，toString方法，equals方法，hashCode方法，不带参数的构造方法
public class Dog {
    private int dno;
    private String dname;
    @Value("${com.source.dog.myskill}")
    //属性名不同，添加注解，前缀也需写全：@Value("${com.source.dog.myskill}")
    private String skill;
}
