package com.example.springboottest.mapper;

import com.example.springboottest.dto.PersonDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
public interface PersonMapper {
    List<PersonDto> showAllPerson();
    void delPerson(int pid);
    void upPerson(PersonDto person);
    void batchUp(Map map);
    void insert(PersonDto person);
    void insertlist(Map map);
    PersonDto showbyid(int pid);
}
