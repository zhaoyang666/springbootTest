package com.example.springboottest.config;

import com.example.springboottest.dto.PersonDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Configuration中所有带@Bean注解的方法都会被动态代理，调用该方法返回的都是同一个实例
 */
@Configuration
public class GetBean {
    @Bean(name = "person1")
    public PersonDto getPerson(){
        return new PersonDto(6,"a66","哈哈");
    }

    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
