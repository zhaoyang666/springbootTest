package com.example.springboottest.config;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * 异步方法类
 */
@Component
public class AsyncTask {
    @Async
    public Future<String> task1() throws InterruptedException {
        long begintime = System.currentTimeMillis();
        Thread.sleep(1000);
        long endtime = System.currentTimeMillis();
        System.out.println("task1耗时："+(endtime-begintime)+"ms");
        return new AsyncResult<String>("task1执行完毕");
    }
    @Async
    public Future<String> task2() throws InterruptedException {
        long begintime = System.currentTimeMillis();
        Thread.sleep(2000);
        long endtime = System.currentTimeMillis();
        System.out.println("task2耗时："+(endtime-begintime)+"ms");
        return new AsyncResult<String>("task2执行完毕");
    }
    @Async
    public  Future<String> task3() throws InterruptedException {
        long begintime = System.currentTimeMillis();
        Thread.sleep(3000);
        long endtime = System.currentTimeMillis();
        System.out.println("task1耗时："+(endtime-begintime)+"ms");
        return new AsyncResult<String>("task3执行完毕");
    }
}
