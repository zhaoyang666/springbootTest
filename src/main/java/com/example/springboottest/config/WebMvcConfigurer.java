package com.example.springboottest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 注册拦截器
 */
//@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册自定义拦截器，添加拦截路径和排除拦截路径
        //拦截器按顺序执行
        //拦截器1拦截路径
        registry.addInterceptor(new OneInterceptor()).addPathPatterns("/showPerson");
        //拦截器2排除的拦截路径
        registry.addInterceptor(new TwoInterceptor()).excludePathPatterns("/index");
        super.addInterceptors(registry);
    }
}
