package com.example.springboottest;

import com.example.springboottest.service.IndexService;
import com.example.springboottest.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {
    @Resource
    private IndexService indexService;
    @Resource
    private PersonService personService;
    @Test
    public void test1(){
        indexService.indexMethod();
    }
    @Test
    public void test2(){
        personService.showAllPerson();
    }

}
